import React from 'react'

export default class Dropdown extends React.Component {

	constructor(props) {
		super(props);
		this.toggleDropdown = this.toggleDropdown.bind(this);
		this.selectOption = this.selectOption.bind(this);
		this.renderOptions = this.renderOptions.bind(this);
		this.state = {
			options: [{name:'Maker', id:'carmake'}, {name:'Model', id:'carmodel'}, {name:'Year', id:'caryear'}, {name:'No Filter', id:null}],
			selectedFilter: 'No Filter',
			isOpen: false
		}
	}

	toggleDropdown() {
		if(this.state.isOpen) {
			this.setState ({
            	isOpen: false
        	})
		}
		else {
			this.setState ({
            	isOpen: true
        	})
		}
	}

	selectOption(item, e) {
		this.setState ({
        	isOpen: false,
        	selectedFilter: item.name
    	})
    	this.props.setFilter(item.id)
	}

	renderOptions(item) {
		return (
			<li>
				<a href='#' onClick={(e) => this.selectOption(item, e)}>{item.name}</a>
			</li>
		)
	}

	render() {
		const isOpen = this.state.isOpen
		return (
			<div className='searchbar-dropdown'>
                <input type='button' value={this.state.selectedFilter} onClick={this.toggleDropdown}/>
                {
                	this.state.isOpen &&
                	<ul className='dropdown-menu'>
            			{this.state.options.filter(options => options.name !== this.state.selectedFilter).map(this.renderOptions, this)}
        			</ul>
                }
            </div>
		)
	}
}