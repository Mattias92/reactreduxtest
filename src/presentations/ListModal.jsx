import React from 'react'

export default class ListModal extends React.Component {

	constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
        this.onUpdate = this.onUpdate.bind(this)
        this.onCreate = this.onCreate.bind(this)
        this.state = {
            item: {id:null, carmake:null, carmodel:null, caryear:null}
        }
    }

    handleChange(e) {
		const item = this.state.item
		const newItem = Object.assign({}, item, {[e.target.name]: e.target.value})
    	this.setState({
    		item: newItem
    	});
    }

    onUpdate() {
        this.props.updateItem(this.state.item)
    }

    onCreate() {
    	const item = this.state.item
    	if(item.carmake != null && item.carmodel != null && item.caryear != null) {
    		this.props.createItem(item)
    	}
    }

    componentWillReceiveProps(nextProps) {
    	this.setState({
        	item: nextProps.item || {id:null, carmake:null, carmodel:null, caryear:null} 
    	})
    }

	render() {
		if (this.props.isOpen === false)
      		return null

		return (
			<div>
				<div className='modal-backdrop' onClick={e => this.close(e)}/>
				<div className='modal-content'>
					{
						this.props.mode == 'edit' &&
						<h1>Edit</h1>
					}
					{
						this.props.mode == 'create' &&
						<h1>Create</h1>
					}
					<form>
						<ul>
							<li>
								<label><strong>Maker</strong></label>
								<input value={this.state.item.carmake} name='carmake' onChange={this.handleChange}/>
							</li>
							<li>
								<label><strong>Model</strong></label>
								<input value={this.state.item.carmodel} name='carmodel' onChange={this.handleChange}/>
							</li>
							<li>
								<label><strong>Year</strong></label>
								<input value={this.state.item.caryear} name='caryear' onChange={this.handleChange}/>
							</li>
						</ul>
					</form>
					<input type='button' className='modal-content-button btn-cancel' value='Cancel' onClick={e => this.close(e)}/>
					{
						this.props.mode == 'edit' &&
						<input type='button' className='modal-content-button btn-confirm' value='Update' onClick={this.onUpdate}/>
					}
					{
						this.props.mode == 'create' &&
						<input type='button' className='modal-content-button btn-confirm' value='Create' onClick={this.onCreate}/>
					}
				</div>
			</div>
		)
	}

	close(e) {
    	e.preventDefault()
		if (this.props.isOpen)	{
			this.props.close()
    	}
  	}
}