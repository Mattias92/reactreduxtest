import React from 'react'
import Dropdown from './Dropdown.jsx'

export default class FilterableSearchbar extends React.Component {

	constructor(props) {
        super(props);
    }

    render() {
    	return (
    		<div>
    			<form className='searchbar'>
                    <Dropdown setFilter={this.props.setFilter}/>
                    <input type='search' placeholder='Search' onChange={this.props.setSearch}/>
                </form>
    		</div>
    	)
    }
}