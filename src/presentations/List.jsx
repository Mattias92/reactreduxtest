import React from 'react'
import ListItem from './ListItem.jsx'
import Dropdown from './Dropdown.jsx'
import FilterableSearchbar from './FilterableSearchbar.jsx'

export default class List extends React.Component {

    constructor(props) {
        super(props);
        this.setFilter = this.setFilter.bind(this)
        this.search = this.search.bind(this)
        this.state = {
            searchresult: this.props.list,
            filter: null
        }
    }

    componentDidMount() {
        //this.props.loadAllItems();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            searchresult: nextProps.list
        })
    }

    setFilter(filter) {  
        this.setState({
            filter: filter
        })
    }

    search(e) {
        if(e.target.value.length > 0) {
            var str = e.target.value.toString().toLowerCase().trim()
            if(this.state.filter == null) {
                this.setState({
                    searchresult: this.props.list.filter(item => Object.values(item).toString().toLowerCase().trim().includes(str))
                })
            }
            else {
                this.setState({
                    searchresult: this.props.list.filter(item => item[this.state.filter].toString().toLowerCase().trim().includes(str))
                })
            }
        }
        else {
            this.setState({
                searchresult: this.props.list
            })
        }
    }

    renderItems(i) {
        return <ListItem item={i} edit={this.props.openListModalEdit} delete={this.props.delete}/>
    }

    render() {
        return (
            <div className='test'>
                <div className='nav'>
                    <div className='nav-header'>
                        <h2>React Crud Test</h2>
                    </div>
                    <div className='nav-search'>
                        <FilterableSearchbar setFilter={this.setFilter} setSearch={this.search}/>
                    </div> 
                    <div className='nav-create'>
                        <input type='button' value='Create Item' className='btn' onClick={this.props.openListModalCreate}/>
                    </div>
                </div>
                <div className='list'>
                        <div className='list-cell'>
                            <strong>Maker</strong>
                        </div>
                        <div className='list-cell'>
                            <strong>Model</strong>
                        </div>
                        <div className='list-cell'>
                            <strong>Year</strong>
                        </div>
                        <div className='list-control-header'>
                            <strong>Edit</strong>
                        </div>
                        <div className='list-control-header'>
                            <strong>Delete</strong>
                        </div>
                        {this.state.searchresult.map(this.renderItems, this)}
                </div>
            </div>
        )
    }
}
