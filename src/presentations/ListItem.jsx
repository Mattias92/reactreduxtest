import React from 'react'

export default class ListItem extends React.Component {

    constructor(props) {
        super(props)
        this.onEdit = this.onEdit.bind(this)
        this.onDelete = this.onDelete.bind(this)
    }

    onEdit(e) {
        this.props.edit(this.props.item)
    }

    onDelete(e) {
        this.props.delete(this.props.item.id)
    }

    render() {
        return (
            <div>
                <div className='list-cell'>
                    {this.props.item.carmake}
                </div>
                <div className='list-cell'>
                    {this.props.item.carmodel}
                </div>
                <div className='list-cell'>
                    {this.props.item.caryear}
                </div>
                <div className='list-control'>
                    <input type='button' value='Edit' className='btn' onClick={this.onEdit}/>
                </div>
                <div className='list-control'>
                    <input type='button' value='Delete' className='btn' onClick={this.onDelete}/>
                </div>
            </div>
        )
    }
}
