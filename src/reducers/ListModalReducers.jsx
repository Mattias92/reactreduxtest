const initialState = {isOpen: false, mode:null, item: null}

export default (state = initialState, action) => {
    switch(action.type) {

        case 'OPEN_LISTMODAL_EDIT':
        	return Object.assign({}, state, {isOpen: true, mode:'edit', item: action.item})

        case 'OPEN_LISTMODAL_CREATE':
        	return Object.assign({}, state, {isOpen: true, mode:'create', item: null})

        case 'CLOSE_LISTMODAL':
        	return Object.assign({}, state, {isOpen: false, mode:null, item: null})

        default:
            return state
    }
}