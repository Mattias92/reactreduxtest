import { combineReducers } from 'redux'
import ListReducer from './ListReducers.jsx'
import ListModalReducer from './ListModalReducers.jsx'

export default combineReducers({
    ListReducers : ListReducer,
    ListModalReducers : ListModalReducer
});
