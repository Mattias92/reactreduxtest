const initialState = []
var idCounter = 0

export default (state = initialState, action) => {
    switch(action.type) {
        case 'CREATE_ITEM':
            //return state.concat(Object.assign({}, action.item, { id: Math.max.apply(Math, state.map((i) => { return i.id })) + 1 }))
            return state.concat(Object.assign({}, action.item, { id: idCounter++}))
        case 'GET_ALL_ITEMS':
            return [
                ...state,
                ...action.array
            ]
        case 'UPDATE_ITEM':
            return state.map((i) => {
                if(i.id !== action.item.id) {
                    return i;
                }
                return Object.assign({}, i, action.item)
            })
        case 'DELETE_ITEM':
            return state.filter(({id}) => id !== action.id)
        default:
            return state
    }
}
