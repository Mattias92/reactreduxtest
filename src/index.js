import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import configureStore from './store/configureStore.jsx'
let store = configureStore()

import ListC from './containers/ListC.jsx'
import ListModalC from './containers/ListModalC.jsx'

class Root extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <div>
                    <ListC />
                    <ListModalC />
                </div>
            </Provider>
        )
    }
}

const container = document.getElementById('container')
ReactDOM.render(<Root />, container)
