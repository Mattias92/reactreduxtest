import { connect } from 'react-redux'
import { loadAllItems, deleteItem, editItem} from '../actions/ListActions.jsx'
import { openListModalEdit, openListModalCreate } from '../actions/ListModalActions.jsx'
import List from '../presentations/List.jsx'

function mapStateToProps(state) {
    return {
        list: state.ListReducers
    }
}

function mapDispatchToProps(dispatch) {
    return {
        //loadAllItems() {
        //    dispatch(loadAllItems("../../mock_data.json"))
        //},
        openListModalEdit(item) {
            dispatch(openListModalEdit(item))
        },
        openListModalCreate() {
            dispatch(openListModalCreate())
        },
        delete(id) {
            dispatch(deleteItem(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)
