import { connect } from 'react-redux'
import { closeListModal } from '../actions/ListModalActions.jsx'
import { updateItem, createItem } from '../actions/ListActions.jsx'
import ListModal from '../presentations/ListModal.jsx'

function mapStateToProps(state) {
    return {
        isOpen: state.ListModalReducers.isOpen,
        mode: state.ListModalReducers.mode,
        item: state.ListModalReducers.item
    }
}

function mapDispatchToProps(dispatch) {
    return {
        close() {
        	dispatch(closeListModal())
        },
        updateItem(item) {
        	dispatch(updateItem(item))
        	dispatch(closeListModal())
        },
        createItem(item) {
        	dispatch(createItem(item))
        	dispatch(closeListModal())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListModal)