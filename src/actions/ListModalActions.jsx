export function openListModalEdit(item) {
	return {
		type: 'OPEN_LISTMODAL_EDIT',
		item 
	}
}

export function openListModalCreate() {
	return {
		type: 'OPEN_LISTMODAL_CREATE'
	}
}

export function closeListModal() {
	return {
		type: 'CLOSE_LISTMODAL'
	}
}

