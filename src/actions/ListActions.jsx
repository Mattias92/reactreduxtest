//export function loadAllItems(path) {
//    return function (dispatch) {
//        return fetch(path).then(function (response) {
//            return response.json()
//                .then(function (json) {
//                    dispatch(getAllItems(json))
//                })
//        }
//    }
//}

export function getAllItems(array) {
    return {
        type: 'GET_ALL_ITEMS',
        array
    }
}

export function createItem(item) {
    return {
        type: 'CREATE_ITEM',
        item
    }
}

export function updateItem(item) {
    return {
        type: 'UPDATE_ITEM',
        item
    }
}

export function deleteItem(id) {
    return {
        type: 'DELETE_ITEM',
        id
    }
}
